export const environment = {
  production: true,
  cookies: {
    currentUser: 'current_user'
  },
  serverURL: 'http://localhost:8080/',
  productTypes: {
    cartoons: 'cartoons',
    single: 'single'
  }
};
