import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {CommonService} from "../../core/service/common-service/common.service";
import {Services} from "../../core/service/common-service/services";
import {takeUntil} from "rxjs/operators";
import {PriceService} from "../../core/service/common-service/http/services/price.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy{

  destory = new Subject();
  products: any = [];
  cartProducts: any [] = [];
  calculatedResponse:any = {};
  env = environment;
  constructor( private service: CommonService, private priceService: PriceService) { }

  ngOnInit() {
    this.getAllProducts();
  }

  ngOnDestroy(): void {
    this.destory.next();
    this.destory.complete();
  }

  getAllProducts() {
    this.service.findAll( Services.PRODUCT, 0).pipe(takeUntil(this.destory)).subscribe( ( response ) => {
      this.products = response;
    })
  }

  addToCart(product){
    product.index = this.cartProducts.length;
    product.type = environment.productTypes.cartoons;
    this.cartProducts.push( {...product} );
  }
  remove(index){
    this.cartProducts.splice( index , 1);
  }
  calculatePrice() {

    if ( this.cartProducts.length <=0 ) {
      alert("Please Add Products to Cart");
      return;
    }

    let products = this.cartProducts.map((product) => {
      return {
        id: product.id,
        qty: product.qty,
        type: product.type,
        index: product.index
      }
    });

    this.priceService.calculatePrice(products).pipe( takeUntil(this.destory)).subscribe( (response :any ) => {

      this.calculatedResponse = response;

      response.lineItems.forEach(( lineItem ) => {

         this.cartProducts.filter((products => products.index === lineItem.index))[0].sub_total = lineItem.sub_total;

      });

    })
  }
  clearCart(){
    this.calculatedResponse = {};
    this.cartProducts = [];
  }
}
